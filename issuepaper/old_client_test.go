package issuepaper

import (
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

func TestGetRootCertificateOld(t *testing.T) {
	assert := assert.New(t)
	port := nextPort()
	closer := startServer(port)
	defer closer()

	*caIssuer = "http://localhost:" + strconv.Itoa(port) + "/v1/"

	cert, err := GetRootCertificate()
	assert.Nil(err)
	assert.Contains(string(cert), "BEGIN CERTIFICATE")
}
