package issuepaper

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"
)

const numAttempts = 10
const waitBetweenAttempts = time.Second * 1

var empty []byte

type rootCertJson struct {
	CertificatePem string `json:"certificate_pem"`
}

type issueCertResponseJson struct {
	CertificatePem string `json:"certificate_pem"`
	PrivateKeyPem  string `json:"private_key_pem"`
}

// Get root certificate and issue a server certificate
func IssueCertificateAndGetRootCertificate(serviceName string) ([]byte, []byte, []byte, error) {
	rootPem, err := GetRootCertificate()
	if err != nil {
		return empty, empty, empty, err
	}
	serverPub, serverPriv, err := IssueCertificate(serviceName)
	if err != nil {
		return empty, empty, empty, err
	}
	return rootPem, serverPub, serverPriv, nil
}

func GetRootCertificate() ([]byte, error) {
	var err error
	var cert []byte
	for r := 0; r < numAttempts; r++ {
		cert, err = getRootCertificateInternal()
		if err == nil {
			return cert, nil
		}
		time.Sleep(waitBetweenAttempts)
	}
	return nil, fmt.Errorf("Cannot obtain root certificate from server: %s", err)
}

func getRootCertificateInternal() ([]byte, error) {
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	url := *caIssuer + "certificates/root"
	req, err := http.NewRequest("GET", url, nil)
	resp, err := client.Do(req)
	if err != nil {
		return empty, fmt.Errorf("Cannot do HTTP GET to %s, %s", url, err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return empty, fmt.Errorf("Cannot read body of POST response from %s, %s", url, err)
	}
	rootCert := &rootCertJson{}
	err = json.Unmarshal(body, rootCert)
	if err != nil {
		return empty, fmt.Errorf("Cannot unmarshal JSON from %s, %s", url, err)
	}
	return []byte(rootCert.CertificatePem), nil
}

func IssueCertificate(serviceName string) ([]byte, []byte, error) {
	var err error
	var cert []byte
	var privateKey []byte
	for r := 0; r < numAttempts; r++ {
		cert, privateKey, err = issueCertificateInternal(serviceName)
		if err == nil {
			return cert, privateKey, nil
		}
		time.Sleep(waitBetweenAttempts)
	}
	return nil, nil, fmt.Errorf("Cannot obtain certificate from server: %s", err)
}

func issueCertificateInternal(serviceName string) ([]byte, []byte, error) {
	client := &http.Client{
		Timeout: time.Second * 10,
	}
	url := *caIssuer + "certificates/" + serviceName
	req, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte("{}")))
	req.Header.Set("Content-Type", "application/json")
	resp, err := client.Do(req)
	if err != nil {
		return empty, empty, fmt.Errorf("Cannot do HTTP POST to %s, %s", url, err)
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return empty, empty, fmt.Errorf("Cannot read body of response from %s, %s", url, err)
	}
	cert := &issueCertResponseJson{}
	err = json.Unmarshal(body, cert)
	if err != nil {
		return empty, empty, fmt.Errorf("Cannot unmarshal JSON from %s, %s", url, err)
	}
	return []byte(cert.CertificatePem), []byte(cert.PrivateKeyPem), nil
}
