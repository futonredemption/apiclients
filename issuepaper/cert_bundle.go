package issuepaper

type ServerCertBundle struct {
	RootCaPem []byte
	PubPem    []byte
	PrivPem   []byte
}

func GetServerCertBundleOrPanic(serviceName string) *ServerCertBundle {
	rootCaPem, pubPem, privPem, err := GetServerAndRootCertificates(serviceName)
	if err != nil {
		panic(err)
	}
	return &ServerCertBundle{
		RootCaPem: []byte(rootCaPem),
		PubPem:    []byte(pubPem),
		PrivPem:   []byte(privPem),
	}
}
