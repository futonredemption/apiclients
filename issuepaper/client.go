package issuepaper

import (
	pb "bitbucket.org/futonredemption/issuepaper/proto/futonredemption"
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"log"
	"time"
)

type IssuePaperClient struct {
	endpoint string
	conn     *grpc.ClientConn
	client   pb.CertificateServiceClient
}

func New() *IssuePaperClient {
	return newWithEndpoint(*caIssuerGrpcAddress)
}

func newWithEndpoint(endpoint string) *IssuePaperClient {
	return &IssuePaperClient{
		endpoint: endpoint,
	}
}

func (this *IssuePaperClient) Open() error {
	if this.conn == nil {
		conn, err := grpc.Dial(this.endpoint, grpc.WithInsecure())
		if err != nil {
			return err
		}
		this.conn = conn
		this.client = pb.NewCertificateServiceClient(conn)
	}
	return nil
}

func (this *IssuePaperClient) Close() error {
	if this.conn != nil {
		err := this.conn.Close()
		if err != nil {
			return err
		}
		this.client = nil
		this.conn = nil
	}
	return nil
}

func (this *IssuePaperClient) GetRootCertificate() (string, error) {
	err := this.Open()
	if err != nil {
		return "", err
	}
	r, err := this.client.GetRootCertificate(context.Background(), &pb.GetRootCertificateRequest{})
	if err != nil {
		return "", err
	}
	return r.CertificatePem, nil
}

func (this *IssuePaperClient) IssueCertificate(serviceName string) (string, string, error) {
	err := this.Open()
	if err != nil {
		return "", "", err
	}
	r, err := this.client.IssueCertificate(context.Background(), &pb.IssueCertificateRequest{
		ServiceName: serviceName,
	})
	if err != nil {
		return "", "", err
	}
	return r.CertificatePem, r.PrivateKeyPem, nil
}

func GetServerAndRootCertificates(serviceName string) (string, string, string, error) {
	var err error

	delay := *caIssuerRetryDelay
	for retries := *caIssuerRetries; retries > 0; retries-- {
		rootCa, pub, priv, err := getServerAndRootCertificatesInternal(serviceName)
		if err == nil {
			return rootCa, pub, priv, nil
		}
		if retries > 1 {
			log.Printf("Could not connect to certificate server, reattempts left %d, %s", retries, err)
			time.Sleep(delay)
		} else {
			log.Printf("Could not connect to certificate server, %s", err)
		}
	}
	return "", "", "", err
}

func getServerAndRootCertificatesInternal(serviceName string) (string, string, string, error) {
	client := New()
	defer client.Close()
	err := client.Open()
	if err != nil {
		return "", "", "", fmt.Errorf("Cannot connect: %s", err)
	}

	rootCa, err := client.GetRootCertificate()
	if err != nil {
		return "", "", "", fmt.Errorf("Cannot obtain root certificate, %s", err)
	}
	pub, priv, err := client.IssueCertificate(serviceName)
	if err != nil {
		return "", "", "", fmt.Errorf("Cannot obtain server certificate, %s", err)
	}
	return rootCa, pub, priv, err
}
