package issuepaper

import (
	"github.com/stretchr/testify/assert"
	"strconv"
	"testing"
)

func TestGetRootCertificate(t *testing.T) {
	assert := assert.New(t)
	port := nextPort()
	closer := startServer(port)
	defer closer()

	*caIssuerGrpcAddress = "localhost:" + strconv.Itoa(port+2)

	client := New()

	cert, err := client.GetRootCertificate()
	assert.Nil(err)
	assert.Contains(string(cert), "BEGIN CERTIFICATE")
}

func TestGetServerAndRootCertificates(t *testing.T) {
	assert := assert.New(t)
	port := nextPort()
	closer := startServer(port)
	defer closer()

	*caIssuerGrpcAddress = "localhost:" + strconv.Itoa(port+2)

	rootCa, pub, priv, err := GetServerAndRootCertificates("test")
	assert.Contains(string(rootCa), "BEGIN CERTIFICATE")
	assert.Contains(string(pub), "BEGIN CERTIFICATE")
	assert.Contains(string(priv), "BEGIN RSA PRIVATE KEY")
	assert.Nil(err)
}
