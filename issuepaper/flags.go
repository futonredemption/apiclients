package issuepaper

import (
	"flag"
	"time"
)

var caIssuer = flag.String("ca.issuer", "http://localhost:11000/v1/", "API Endpoint for obtaining security certificates")
var caIssuerGrpcAddress = flag.String("ca.issuer.grpcaddress", "localhost:11002", "Certificate Authority Endpoint that is addressible by gRPC protocol.")
var caIssuerRetries = flag.Int("ca.issuer.retries", 10, "Number of retry attempts to obtain a certificate from the Certificate Authority before giving up.")
var caIssuerRetryDelay = flag.Duration("ca.issuer.retrydelay", time.Second, "Delay between each retry attempt to obtain a security certificate from the Certificate Authority.")
