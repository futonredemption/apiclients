package issuepaper

import (
	"bitbucket.org/futonredemption/issuepaper/kms"
	"bitbucket.org/futonredemption/issuepaper/server"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
)

var port = 20000

func createTestDir() (string, func()) {
	dir, err := ioutil.TempDir("", "app_test")
	if err != nil {
		panic(err)
	}
	return dir, func() {
		os.RemoveAll(dir)
	}
}

func startServer(port int) func() {
	dir, closer := createTestDir()
	dbPath := filepath.Join(dir, "test.db")
	keyBundle, err := kms.New(dbPath, genHostsForCert(port), "Test", "Test")
	if err != nil {
		panic(err)
	}

	kmsService, err := server.NewIssuePaperService(keyBundle)
	if err != nil {
		log.Fatalf("Cannot create service, %s", err)
	}
	server := server.SetupForTest(kmsService, port)
	server.BeginServing()
	server.WaitForServing()

	return func() {
		server.StopServing()
		closer()
	}
}

func genHostsForCert(port int) string {
	hosts := "localhost"
	for i := port; i < port+5; i++ {
		hosts += ",:" + strconv.Itoa(i)
	}
	return hosts
}

func nextPort() int {
	p := port
	port += 5
	return p
}
