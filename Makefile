prefix = /usr
bindir = $(prefix)/bin
sharedir = $(prefix)/share
mandir = $(sharedir)/man
man1dir = $(mandir)/man1
GO := @go
GOGET := go get -u -d
GOGETBUILD := go get -u
SOURCE_DIRS=$(shell go list ./... | grep -v '/vendor/')
export PATH := $(PWD)/bin:$(PATH):/usr/local/go/bin:/usr/go/bin
BINARY_NAME=apiclients
MAN_PAGE_NAME=${BINARY_NAME}.1
SERVER_MAIN=${BINARY_NAME}.go

all: test

lint:
	$(GO) fmt ${SOURCE_DIRS}
	$(GO) vet ${SOURCE_DIRS}

clean:
	@rm -f ${BINARY_NAME} ${BINARY_NAME}-* *.pem release.tar.gz
	@rm -rf release/ web/bindata_assetfs.go coverage.txt

check: test

test: build-deps lint
	$(GO) test ${SOURCE_DIRS} -race

cover: build-deps
	$(GO) test -cover ${SOURCE_DIRS} -race

coverage.txt: build-deps
	$(GO) test -coverprofile=coverage.txt -covermode count ${SOURCE_DIRS}

bench: benchmark

benchmark: build-deps
	$(GO) test -cover -benchmem -bench=. ${SOURCE_DIRS}

deps:
	# gRPC
	$(GOGET) google.golang.org/grpc/...
	$(GOGETBUILD) github.com/golang/protobuf/proto
	$(GOGETBUILD) github.com/golang/protobuf/protoc-gen-go
	$(GOGETBUILD) github.com/grpc-ecosystem/grpc-gateway/...
	
	# Prometheus
	$(GOGET) github.com/prometheus/client_golang/...
	$(GOGET) github.com/grpc-ecosystem/go-grpc-prometheus/...

	# Testing
	$(GOGETBUILD) github.com/t-yuki/gocover-cobertura
	$(GOGET) github.com/stretchr/testify/...

project-deps:
	$(GOGET) bitbucket.org/futonredemption/issuepaper

tools:
	$(GOGETBUILD) golang.org/x/tools/cmd/gorename
	$(GOGETBUILD) github.com/golang/lint/golint
	$(GOGETBUILD) github.com/nsf/gocode
	$(GOGETBUILD) github.com/rogpeppe/godef
	$(GOGETBUILD) github.com/lukehoban/go-outline
	$(GOGETBUILD) github.com/newhook/go-symbols
	$(GOGETBUILD) github.com/sqs/goreturns

.PHONY : all build build-deps lint clean check test cover bench benchmark deps tools
