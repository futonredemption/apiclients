package log

import (
	"flag"
)

var catalogServerAddressFlag = flag.String("logging.grpcaddress", "localhost:11012", "Catalog Server Endpoint for logging.")
