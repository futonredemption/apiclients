package log

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestLogging(t *testing.T) {
	assert := assert.New(t)
	Debug("debug")
	Info("info")
	Warning("warning")
	Error("error")
	Fatal("fatal")
	assert.Nil(Close())
}
