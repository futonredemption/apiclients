package log

import (
	pb "bitbucket.org/futonredemption/catalogserver/proto/futonredemption"
	"fmt"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	logging "log"
)

type catalogClient struct {
	endpoint string
	conn     *grpc.ClientConn
	client   pb.CatalogServiceClient
}

var globalClient *catalogClient

func initializeLogging() {
	initializeLoggingWithEndpoint(*catalogServerAddressFlag)
}

func initializeLoggingWithEndpoint(endpoint string) {
	if globalClient == nil {
		conn, err := grpc.Dial(endpoint, grpc.WithInsecure())
		if err != nil {
			logging.Printf("Cannot connect to logging service: %s", err)
			return
		}
		client := &catalogClient{
			endpoint: endpoint,
			conn:     conn,
			client:   pb.NewCatalogServiceClient(conn),
		}
		globalClient = client
	}
}

func Printf(format string, v ...interface{}) {
	Print(fmt.Sprintf(format, v...))
}

func Debugf(format string, v ...interface{}) {
	Debug(fmt.Sprintf(format, v...))
}

func Infof(format string, v ...interface{}) {
	Info(fmt.Sprintf(format, v...))
}

func Warningf(format string, v ...interface{}) {
	Warning(fmt.Sprintf(format, v...))
}

func Errorf(format string, v ...interface{}) {
	Error(fmt.Sprintf(format, v...))
}

func Fatalf(format string, v ...interface{}) {
	Fatal(fmt.Sprintf(format, v...))
}

func Print(msg string) {
	logMethod("[INFO] %s", msg, pb.LogSeverity_INFO)
}

func Debug(msg string) {
	logMethod("[DEBUG] %s", msg, pb.LogSeverity_DEBUG)
}

func Info(msg string) {
	logMethod("[INFO] %s", msg, pb.LogSeverity_INFO)
}

func Warning(msg string) {
	logMethod("[WARN] %s", msg, pb.LogSeverity_WARNING)
}

func Error(msg string) {
	logMethod("[ERROR] %s", msg, pb.LogSeverity_ERROR)
}

func Fatal(msg string) {
	logMethod("[FATAL] %s", msg, pb.LogSeverity_FATAL)
}

func logMethod(prefix string, msg string, severity pb.LogSeverity) {
	initializeLogging()
	logging.Printf(prefix, msg)
	if globalClient != nil {
		globalClient.client.Log(context.Background(), &pb.LogRequest{
			Entry:    messageArray(msg),
			Severity: severity,
		})
	}
}

func Close() error {
	if globalClient != nil {
		err := globalClient.conn.Close()
		if err != nil {
			err = fmt.Errorf("[ERROR] Cannot disconnect from catalog service, %s", err)
			logging.Printf("%s", err)
			return err
		}
	}
	globalClient = nil
	return nil
}

func messageArray(msg string) []string {
	return []string{msg}
}
